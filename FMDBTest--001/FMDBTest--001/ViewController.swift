//
//  ViewController.swift
//  FMDBTest--001
//
//  Created by FelixYin on 15/8/3.
//  Copyright © 2015年 felixios. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()


//        selectAllRecord()
        
//        insertOneRecordByQueue()
        
//          insertManyRecordByQueueAndTransation()
        
          selectAllRecordByQueue()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
//    插入一条数据
    
    
    private func insertOneRecord() ->Void{
        
        
      //增加，删除，更新都是使用executeUpdate，唯一的区别就是sql语句的不同
    
      let sql = "insert into T_Dog (name,age) values('小黄',12);"
        
//      let sql = "delete from T_Dog where dogid = 1"
        
//      let sql = "update T_Dog set name = '修改修改' where dogid = 2;"

        
      if !SQLiteManager.sharedManager.db!.executeUpdate(sql) {
        
           print("插入失败")
        
           return
        
        }
        
        
        print("插入成功!")
    
    
    }
    
    
    
//    使用线程向数据库中插入一条数句,没有使用事务
    
    private func insertOneRecordByQueue() ->Void{
        
     //增加,删除,修改都是使用executeUpdate，唯一的区别就是sql语句的不同
    
    let sql = "insert into T_Dog (name,age) values('小黄黄',13)"
        
//    let sql = "delete from T_Dog where dogid = 1;"
        
//      let sql = "update T_Dog set name = '修改修改' where dogid = 2;"
        
        
    SQLiteManager.sharedManager.dbQueue?.inDatabase({ (db) -> Void in
        
        
        print("插入数据执行线程\(NSThread.currentThread())")  //这样输出结果是在主线程上
        
        
        if !db.executeUpdate(sql) {
        
          
            print("插入失败")
            
            return
            
        }
        
            print("插入成功")
        
        
    })
    
    
    
    }
    
    
//    使用线程，事务向数据库中插入多条数据
    
    //如果应用中使用了多线程操作数据库，那么就需要使用FMDatabaseQueue来保证线程安全了
    
    private func insertManyRecordByQueueAndTransation() ->Void{
    
       let sql = "insert into T_DOG (name,age) values(?,?);"
        
        
       SQLiteManager.sharedManager.dbQueue?.inTransaction({ (db, isRollBack) -> Void in
        
        let start = CFAbsoluteTimeGetCurrent()
        
//        print("使用事务插入多条数据的当前线城是\(NSThread.currentThread())")  //在主线程   0.06
        
        for i in 0..<5000{
        
                if !db.executeUpdate(sql,"dog\(i)",i+100) {
                    
                    print("插入多条数据失败")
                    
                    //出现问题，需要回滚
                    isRollBack.memory = true
                    
                    return
                    
                }
            
//            print("使用事务插入多条数据的当前线城是\(NSThread.currentThread())")  //在主线程
            
//                print("插入成功!")
            
        }
        
        
        print("耗时---\(CFAbsoluteTimeGetCurrent() - start)")
        
        
       })
    
    
    
    }
    
    
//   使用线程安全的FMDatabaseQueue查询数据
    
    
    private func selectAllRecordByQueue() ->Void{
    
      let sql = "select name,age,dogid from T_Dog;"
        
      SQLiteManager.sharedManager.dbQueue?.inTransaction({ (db, isRollBack) -> Void in
        
        
        let start = CFAbsoluteTimeGetCurrent()
        
        //执行查询
        if let result = db.executeQuery(sql, withArgumentsInArray: nil) {
        
        //result中包含了查询返回的结果集合，所以需要遍历取出集合，next()都是拿出一条记录，也就是row
            
            while result.next() {
            
                  //取出单个记录的值，也就是col中的值
                
//                let name = result.stringForColumn("name")
//                
//                let age = result.intForColumn("age")
//                
//                let dogid = result.intForColumn("dogid")
                
//                print("name-->\(name) age-->\(age)  dogid-->\(dogid)")
                
            }
        
        }
        
        
        print("耗时---\(CFAbsoluteTimeGetCurrent() - start)")
        
        
      })
    
    
    
    }
    
    
    
//    查询数据
    
    
    private func selectAllRecord() ->Void{
    
    
      let sql = "select name,age from T_Dog;"
        
        
        //注意：使用执行查询语句会直接崩溃 SQLiteManager.sharedManager.db!.executeUpdate(sql)
        
        let start = CFAbsoluteTimeGetCurrent()
        
        
        if let result = SQLiteManager.sharedManager.db!.executeQuery(sql, withArgumentsInArray: nil) {
        
            
            print("执行查询语句成功")
            
            
            //遍历结果
            
            while result.next() {
                
//              let name = result.stringForColumn("name")
//                
//              let age = result.intForColumn("age")
            
//              print("name-->\(name)   age-->\(age)")
            
            
            }
            
        
        }
        
        
        
        print("耗时\(CFAbsoluteTimeGetCurrent() - start)")
      
    
    
    
    
    }


}

