//
//  SQLiteManager.swift
//  FMDBTest--001
//
//  Created by FelixYin on 15/8/3.
//  Copyright © 2015年 felixios. All rights reserved.
//

import UIKit

class SQLiteManager: NSObject {
    
    //数据库管理单例
    private static let instance:SQLiteManager = SQLiteManager()
    
    //数据操作对象，相当于一个句柄
    
    var db:FMDatabase?
    
    var dbQueue:FMDatabaseQueue?
    
    class var sharedManager:SQLiteManager {
    
       return instance
    }
    
    
    
    //创建一个数据库
    
    func openDB(dbName:String) ->Void {
    
       let path = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true).last!.stringByAppendingPathComponent(dbName)
        
        print(path)
       
       //创建一个操作数据库的对象---> 这一部执行完后，dbName还没有被创建
        
       db = FMDatabase(path: path)
        
       //打开数据库，如果不存在，创建一个新的数据库，存在的话直接打开
        
        if !db!.open() {
        
            print("打开失败")
            
            return
        
        }
        
        
        print("打开成功")
        
        
        //成功之后创建一张表
        
        self.createTable()
    
    }
    
    
    //使用FMDatabaseQueue操作数据库
    
    //如果应用中使用了多线程操作数据库，那么就需要使用FMDatabaseQueue来保证线程安全了
    
    
    func openDBByQueue(dbName:String) ->Void{
        
        let path = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true).last!.stringByAppendingPathComponent(dbName)
        
        print(path)
    
        //创建一个FMDatabaseQueue对象，此对象一创建，紧跟着数据库也会被创建出来，数据库也会被打开   与普通操作不同
        
         dbQueue = FMDatabaseQueue(path: path)
        
        
        //无需打开数据库
    
        
        
        //创建一张表
        
        createTableByQueue()
        
        
    }
    
    
    //创建一张表
    
    private func createTable() ->Bool{
        
        let sql = "CREATE TABLE IF NOT EXISTS 'T_Dog' (\n" +
            "'NAME' TEXT,\n" +
            "'AGE' INTEGER,\n" +
            "'DOGID' INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT" +
        ");"
    
        if !db!.executeUpdate(sql, withArgumentsInArray:nil) {
        
           print("创建表失败")
            
           return false
        
        }
        
        
          print("创建表成功")
        
          return true
    
    
    }
    
    
    //创建一张表使用FMDatabaseQueue
    
    //如果应用中使用了多线程操作数据库，那么就需要使用FMDatabaseQueue来保证线程安全了，从而保证了数据库文件中数据的安全
    
    
    private func createTableByQueue() ->Void{
        
        /*
        
          创建表格的标准写法：id是自动增长的   注意在拼接的时候最后加\n，以便后面的调试
        
        
        CREATE TABLE 'T_Car' (
        'CARNAME' TEXT,
        'CARBRAND' TEXT,
        'CARID' INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT
        )
        
        
        
        */
        
        
        
    
//        let sql = "CREATE TABLE IF NOT EXISTS T_Dog (NAME,AGE) "
        
        let sql = "CREATE TABLE IF NOT EXISTS 'T_Dog' (\n" +
        "'NAME' TEXT,\n" +
        "'AGE' INTEGER,\n" +
        "'DOGID' INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT\n" +
        ");"
        
        
        //在线程里创建一张表
        
        dbQueue!.inDatabase({ (db) -> Void in
            
            
            
            print("创建表的当前线程是\(NSThread.currentThread())")   //输出结果是在主线程
            
            
            if !db.executeUpdate(sql) {
            
                
               print("创建表失败")
                
               return
            
            }
            
//            print("创建表的当前线程是\(NSThread.currentThread())")  //输出结果是在主线程
            
               print("创建表成功")
            
        })
    
    
    
    
    }

}
